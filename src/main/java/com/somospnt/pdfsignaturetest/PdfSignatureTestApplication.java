package com.somospnt.pdfsignaturetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdfSignatureTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(PdfSignatureTestApplication.class, args);
	}
}
